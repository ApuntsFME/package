apuntsfme.tar.gz: apuntsfme.dtx apuntsfme.ins apuntsfme_guide.pdf README
	mkdir -p apuntsfme
	cp README apuntsfme
	cp apuntsfme.ins apuntsfme
	cp apuntsfme.dtx apuntsfme
	cp apuntsfme_guide.pdf apuntsfme
	tar -czf apuntsfme.tar.gz apuntsfme

apuntsfme_guide.pdf: apuntsfme.dtx apuntsfme.sty
	pdflatex -jobname="apuntsfme_guide" "\AtBeginDocument{\OnlyDescription}\input apuntsfme.dtx"	
	makeindex apuntsfme_guide
	pdflatex -jobname="apuntsfme_guide" "\AtBeginDocument{\OnlyDescription}\input apuntsfme.dtx"	

apuntsfme.sty: apuntsfme.ins apuntsfme.dtx
	yes | pdflatex apuntsfme.ins
	

.PHONY: clean
clean:
	rm -rf apuntsfme apuntsfme.tar.gz *.aux *.auxlock *.glo *.ind *.idx *.ilg *.log *.out *.cls *.sty apuntsfme_guide.pdf

