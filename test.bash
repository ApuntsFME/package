#!/bin/bash

# Usage: script pdf1 pdf2

convert -density 96 $1 -colorspace RGB a.png
echo "Finished conversion of $1"
convert -density 96 $2 -colorspace RGB b.png
echo "Finished conversion of $2"

# For output files
for i in b-*.png; do
    out=$(magick compare -metric PSNR ${i} a${i:1} diff.png 2>&1)
    #echo "checking page ${i:2:${#i}-6}"
    if [[ "$out" != "inf" ]] && [[ $out < 45 ]]; then
        echo "Error, difference in page ${i:2:${#i}-6} diff.png"
        while true; do
            read -p "Do you wish to ignore this and continue?" yn
            case $yn in
                [Yy]* ) break;;
                [Nn]* ) break 3;;
                * ) echo "Please answer yes or no.";;
            esac
        done
    fi
done

rm -rf b-*.png a-*.png
